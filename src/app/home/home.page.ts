import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  newItem: string;

  constructor(private todoService: TodoService, private router: Router) {}

  ngOnInit() {

  }

  add() {
    this.router.navigateByUrl('task');
  }
}
